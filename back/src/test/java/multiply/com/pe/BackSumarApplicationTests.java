package multiply.com.pe;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import sumarback.com.pe.BackSumarApplication;

@RunWith(MockitoJUnitRunner.class)
public class BackSumarApplicationTests {

	@Test
	public void contextLoads() {
		BackSumarApplication application = new BackSumarApplication();
		assertNotNull(application);
	}

}
