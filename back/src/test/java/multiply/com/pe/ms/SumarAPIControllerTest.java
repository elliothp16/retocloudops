package multiply.com.pe.ms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import sumarback.com.pe.controller.SumarAPIController;
import sumarback.com.pe.objeto.Response;
import sumarback.com.pe.service.SumarService;

@RunWith(MockitoJUnitRunner.class)
public class SumarAPIControllerTest {

	@InjectMocks
	SumarAPIController sumarSimpleRestService;

	@Mock
	SumarService multiplyService;
	
	@Test
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

		@Test
		public void PruebaLetras()  {	
			String EMPTY="",param_one="a",param_two="2";  
			Response respose=sumarSimpleRestService.sumar(param_one,param_two);		
			 assertNotNull(respose.getComment());
			 System.out.println(respose.getResult());
			 assertEquals(StringUtils.contains(respose.getComment(), "Error"),true);
	//
		}
		
		
		public void AccesoControllerTests()  {	
			String EMPTY="",param_one="50",param_two="2";  
			Response respose=sumarSimpleRestService.sumar(param_one,param_two);		
			 assertNotNull(respose);
			 System.out.println(respose.getResult());
			 assertEquals(respose.getResult()+EMPTY, "0.0");
	//
		}

}
