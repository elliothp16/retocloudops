package sumarback.com.pe.repository;
 
import org.springframework.data.repository.CrudRepository;
import sumarback.com.pe.entity.SumarTabla;

public interface SumarRepo extends CrudRepository<SumarTabla, Long> { 

}
