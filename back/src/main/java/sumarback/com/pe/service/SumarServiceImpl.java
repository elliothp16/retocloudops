package sumarback.com.pe.service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sumarback.com.pe.objeto.*;
import sumarback.com.pe.entity.SumarTabla;
import sumarback.com.pe.repository.SumarRepo;

@Service
public class SumarServiceImpl implements SumarService {

	@Autowired
	private SumarRepo multiplyRepo;

	ModelMapper modelMapper = new ModelMapper();

	@Override
	public SumarObjeto save(SumarObjeto dto) {

		SumarTabla bean = multiplyRepo.save(modelMapper.map(dto, SumarTabla.class));
		SumarObjeto dtoOut = modelMapper.map(bean, SumarObjeto.class);
		return dtoOut;

	}

}
