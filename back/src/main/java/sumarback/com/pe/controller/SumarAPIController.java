package sumarback.com.pe.controller;

import org.apache.commons.lang3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import sumarback.com.pe.objeto.Response; 
import sumarback.com.pe.objeto.SumarObjeto;
import sumarback.com.pe.service.SumarService;

@RestController
public class SumarAPIController {
	Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	SumarService multiplyService;

	@GetMapping("/sumar/{one}/{two}")
	public Response sumar(@PathVariable(value = "one") String one, @PathVariable(value = "two") String two) {
		if (StringUtils.isNumeric(one) && StringUtils.isNumeric(two)) {
			SumarObjeto out = multiplyService.save(new SumarObjeto(Double.valueOf(one), Double.valueOf(two)));
			if (out == null) {
				return new Response(0, "Error Data");
			} else {
				return new Response(out.getResult(), "Calculated");
			}
		} else {
			return new Response(0, "Error DataType");
		}

	}
}
