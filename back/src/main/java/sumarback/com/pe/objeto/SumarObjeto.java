package sumarback.com.pe.objeto;
 
public class SumarObjeto {

	private Long id;
	private double one;
	private double two;
	private double result;

	public SumarObjeto() {
		super();
	}


	public SumarObjeto(double one, double two) {
		super(); 
		this.one = one;
		this.two = two;
		this.result = one+two;
	}
	public SumarObjeto(Long id, double one, double two, double result) {
		super();
		this.id = id;
		this.one = one;
		this.two = two;
		this.result = one+two;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getOne() {
		return one;
	}

	public void setOne(double one) {
		this.one = one;
	}

	public double getTwo() {
		return two;
	}

	public void setTwo(double two) {
		this.two = two;
	}

	public double getResult() {
		return result;
	}

	public void setResult(double result) {
		this.result = result;
	}

}
